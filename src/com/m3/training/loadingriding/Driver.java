package com.m3.training.loadingriding;

import java.util.Scanner;

public class Driver {

	public static void main(String[] args) {
		// //TODO show try with resources
		try (Scanner scanner = new Scanner(System.in);) {
			System.out.println("Animal repsonse");
			scanner.nextLine();
			Animal.main(args);
			System.out.println("\n\nSnake repsonse");
			scanner.nextLine();
			Snake.main(args);
			System.out.println("\n\nHorse repsonse");
			scanner.nextLine();
			Horse.main(args);
		}
		

	}

}
