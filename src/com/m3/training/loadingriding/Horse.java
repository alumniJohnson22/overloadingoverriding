package com.m3.training.loadingriding;

public class Horse extends Animal {
	protected String name = "Equus";
	public Horse() {
	}
	//overrides the parent toString method
	public String toString() {
		boolean horsey = false;
		return this.toString(horsey);
	}
	//overloads the previous toString method, at line 17
	public String toString(boolean horsey) {
		if (horsey) {
			return this.name;
		}
		return this.getName();
	}
	public String gallop() {
		return "very fast!!!!";
	}
	
	public static void main(String[] args) {
		Horse horse = new Horse();
//		System.out.println(horse);
//		Animal animal = horse;
//		System.out.println(animal.name);
//		horse.gallop(); animal.gallop();
		boolean horsey = true;
		System.out.println(horse.toString(horsey));		
	}
}
