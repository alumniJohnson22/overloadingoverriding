package com.m3.training.loadingriding;
public class Animal {
	protected String name = "Animalia";
	public Animal() {	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String toString() {
		return this.getName();
	}
	public static void main(String[] args) {
		Animal animal = new Animal();
		System.out.println(animal);
	}
}
